

import Vue from 'vue'
import CONF from '../commons/conf'
import {active} from '../commons/authorization';

// Promise 垫片
require('core-js/fn/promise');


let Service = {

    /**
     * Promise请求基础方法
     * @params {String} url 请求的url
     * @params {Object} params 请求的参数
     * @return Promise
     * */
    reqPromise(url, params) {

        return new Promise((resolve, reject) => {
            Vue.http({
                url: url,
                method: 'GET',
                params: params,
                headers: {
                    'Authorization': 'Bearer ' + window.localStorage.access_token,
                },
                //credentials: true
            }).then(data => {

                let code = data.body.code;

                // 无权限查看，去激活账号
                if (code === 401) {

                    if (params.a_secret && params.a_id) {

                        active(params.a_secret, params.a_id).then(actData => {

                            if (!actData.status) {
                                window.location.href="http://www.luomi.shop/#/active/";
                            } else {
                                window.location.href="http://www.luomi.shop/#/active/" + params.id + '/' + params.lessonId;
                            }

                            //typeof params.callback === 'function' && params.callback(actData);
                        });
                    } else {

                        alert('无权限');
                    }
                } else if (code === 200) {
                    resolve(data.body.data);
                }

            }, data => {
                reject(new Error(data));
            });
        });
    },

    /**
     * 获取微信授权的access_token
     * params {String} code 微信授权code
     * params {String} c_secret （业务相关）
     * params {String} c_id (业务相关)
     */
    queryAccessToken(code) {

        return new Promise((resolve, reject) => {

            Vue.http({
                url: CONF.API.tokenApi,
                method: 'GET',
                params: {
                    code: code
                }
            }).then(data => {

                if (data.body.code === 200) {

                    resolve(data.body.data);

                } else {
                    reject(new Error(data.body.message));
                }

            }, data => {

                reject(new Error(data));
            });
        });
    },

    // 获取首页数据
    queryIndexData() {

        return this.reqPromise(CONF.API.index, {});
    },

    // 获取课程列表信息
    queryCourseList(id) {

        return this.reqPromise(CONF.API.courseListApi, {

            category_id: id
        });
    },
    // 获取课程信息
    queryCourse(id, lessonId, a_secret, a_id) {

        return this.reqPromise(CONF.API.courseApi + id, {
            a_secret: a_secret,
            a_id: a_id,
            id: id,
            lessonId: lessonId
        });
    },

    // 获取测验题
    queryExercise(id) {

        return this.reqPromise(CONF.API.exerciseApi, {

            id: id
        });
    },
    /**
     * 上传用户的检测结果
     * @param  {[String]} exercise_id 测试题id
     * @param  {[String]} result_rank 评分
     * @return {[Promise]}
     */
    uploadExerciseResult(exercise_id, result_rank) {
        return new Promise((resolve, reject) => {
            Vue.http({
                url: CONF.API.uploadExerciseApi,
                method: 'POST',
                body: {
                    exercise_id: exercise_id,
                    result_rank: result_rank
                },
                headers: {
                    'Authorization': 'Bearer ' + window.localStorage.access_token,
                }
            }).then(data => {

                if (data.body.code === 200) resolve(data.body.data);

            }, data => {
                reject(new Error(data));
            });
        });
    },

    queryShare(userId, exerciseId) {

        return this.reqPromise(CONF.API.uploadExerciseApi, {

            user_id: userId,
            exercise_id: exerciseId
        });
    },
    //获取微信s d k配置信息
    queryWechatSDK() {
        return this.reqPromise(CONF.API.weChatSDK, {
            url: window.location.href
        });
    },
    recordScore(origin, sentence_learn_id) {

        //alert(origin + sentence_learn_id);

        return new Promise((resolve, reject) => {
            Vue.http({
                url: CONF.API.recordScore,
                method: 'POST',
                body: {
                    origin: origin,
                    sentence_learn_id: sentence_learn_id
                },
                headers: {
                    'Authorization': 'Bearer ' + window.localStorage.access_token,
                }
            }).then(data => {

                if (data.body.code === 200) resolve(data.body.data);

            }, data => {

                //alert(JSON.stringify(data));
                reject(new Error(data));
            });
        });
    },
    /**
     * 上传播放进度
     * @param  {[String]} course_id  课程ID
     * @param  {[String]} lesson_id  课时ID
     * @param  {[String]} watch_time 观看进度（s）
     * @param  {[String]} total_time 视频总时间（s）
     * @return {[Promise]}
     */
    uploadWatchHistory(course_id, lesson_id, watch_time) {

        let _watch_time = watch_time ? parseInt(watch_time) : 0;

        //alert(course_id + '^^^' + lesson_id + '^^^' + watch_time);

        return new Promise((resolve, reject) => {
            Vue.http({
                url: CONF.API.uploadHistory,
                method: 'POST',
                body: {
                    course_id: course_id,
                    lesson_id: lesson_id,
                    watch_time: _watch_time,
                    user_id: 1
                },
                headers: {
                    'Authorization': 'Bearer ' + window.localStorage.access_token,
                }
            }).then(data => {

                //alert(JSON.stringify(data));

                if (data.body.code === 200) resolve(data.body.data);

            }, data => {

                reject(new Error(data));
            });
        });
    },

    /**
     * 获取跟读的题目
     * @param  {[type]} repeat_id [description]
     * @return {[type]}           [description]
     */
    queryRecordQuestion(repeat_id) {
        return this.reqPromise(CONF.API.recordQuestion, {

            repeat_id: repeat_id
        });
    },

    /**
     * 保存跟读记录
     * @param  {[type]} sentence_id [description]
     * @param  {[type]} server_id   [description]
     * @return {[type]}             [description]
     */
    saveRecord(sentence_id, server_id) {

        return new Promise((resolve, reject) => {
            Vue.http({
                url: CONF.API.recordQuestion,
                method: 'POST',
                body: {
                    sentence_id: sentence_id,
                    server_id: server_id
                },
                headers: {
                    'Authorization': 'Bearer ' + window.localStorage.access_token,
                }
            }).then(data => {

                if (data.body.code === 200) resolve(data.body.data);

            }, data => {

                //alert("录音保存失败");
                reject(new Error(data));
            });
        });
    }
};
export default Service;
