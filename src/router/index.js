import Vue from 'vue'
import Router from 'vue-router'
import Index from '@/components/Index'
import Course from '@/components/Course'
import Play from '@/components/Play'
import List from '@/components/List'
import Test from '@/components/Test'
import Result from '@/components/Result'
import Share from '@/components/Share'
import Read from '@/components/Read'
import Me from '@/components/me'
import Active from '@/components/active'

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/index',
            name: 'Index',
            component: Index
        },
        {
            path: '/course/:id',
            name: 'Course',
            component: Course
        },
        {
            path: '/play/:id/:lessonId/:a_secret?/:a_id?',
            name: 'Play',
            component: Play
        },
        {
            path: '/list/:id',
            name: 'List',
            component: List
        },
        {
            path: '/read/:id',
            name: 'Read',
            component: Read
        },
        {
            path: '/test/:id',
            name: 'Test',
            component: Test
        },
        {
            path: '/result/:id',
            name: 'Result',
            component: Result
        },
        {
            path: '/share/:userId/:exerciseId',
            name: 'Share',
            component: Share
        },
        {
            path: '/me',
            name: 'Me',
            component: Me
        },
        {
            path: '/active/:id?/:lessonId?',
            name: 'Active',
            component: Active
        }
    ]
})
