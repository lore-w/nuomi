export default {

    "API": {
        "index": "http://www.luomi.shop:8080/api/index/index-data",
        "tokenApi": "http://www.luomi.shop:8080/oauth/wechat/callback",
        "courseListApi": "http://www.luomi.shop:8080/api/course/index",
        "courseApi": "http://www.luomi.shop:8080/api/course/",
        "exerciseApi": "http://www.luomi.shop:8080/api/course/exercise",
        "uploadExerciseApi": "http://www.luomi.shop:8080/api/exercise/result",
        "weChatSDK": "http://www.luomi.shop:8080/api/wechat/jssdk",
        "checkIsLogin": "http://www.luomi.shop:8080/oauth/wechat/authorize",
        "activeUser": "http://www.luomi.shop:8080/oauth/wechat/activate",
        "recordScore": "http://www.luomi.shop:8080/api/record/score",
        "uploadHistory": "http://www.luomi.shop:8080/api/lesson/learn",
        "recordQuestion": "http://www.luomi.shop:8080/api/lesson/repeat"
    }
    /*"API": {
        "index": "http://www.caodunfu.com:8080/api/index/index-data",
        "tokenApi": "http://www.caodunfu.com:8080/oauth/wechat/callback",
        "courseListApi": "http://www.caodunfu.com:8080/api/course/index",
        "courseApi": "http://www.caodunfu.com:8080/api/course/",
        "exerciseApi": "http://www.caodunfu.com:8080/api/course/exercise",
        "uploadExerciseApi": "http://www.caodunfu.com:8080/api/exercise/result",
        "weChatSDK": "http://www.caodunfu.com:8080/api/wechat/jssdk",
        "checkIsLogin": "http://www.caodunfu.com:8080/oauth/wechat/authorize",
        "activeUser": "http://www.caodunfu.com:8080/oauth/wechat/activate",
        "recordScore": "http://www.caodunfu.com:8080/api/record/score",
        "uploadHistory": "http://www.caodunfu.com:8080/api/lesson/learn",
        "recordQuestion": "http://www.caodunfu.com:8080/api/lesson/repeat"
    }*/
};
