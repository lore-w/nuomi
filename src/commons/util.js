
import {checkLogin, authorize} from './authorization';

let Util = {

    /**
     * 获取url参数
     */

    queryURLParams () {

        let url = location.search, //获取url中"?"符后的字串
            theRequest = new Object();

        if (url.indexOf("?") !== -1) {

            let str = url.substr(1),
                strs = str.split("&");

            for(let i = 0; i < strs.length; i ++) {
                theRequest[strs[i].split("=")[0]]=unescape(strs[i].split("=")[1]);
            }
        }
        return theRequest;
    },

    /**
     * 检查是否登录
     */
    auth (callback) {

        let _this = this;

        checkLogin().then(data => {

            if (data.status) {
                callback()
            } else {
                // 去授权
                authorize().then(data => {

                    if (data.status) _this.auth(callback);
                });
            }
        });
    },

    /**
     * 截取字符串时间
     */
    getStringTime(time) {

        let date = time.split(' '),
            YMD = date[0].split('-'),
            HMS = date[1].split(':'),
            Y = YMD[0],
            M = YMD[1],
            D = YMD[2],
            h = HMS[0],
            m = HMS[1],
            s = HMS[2];

        return {Y, M, D, h, m, s}
    },

    /**
     * 格式化时间
     */
    formatDate(date, connector) {

        return date.getFullYear() + connector + (date.getMonth() + 1) + connector + date.getDate();
    }

};
export {Util};


