import Vue from 'vue'

import {Util} from './util';
import Server from '../services/index';
import CONF from '../commons/conf'

// Promise 垫片
require('core-js/fn/promise');

/**
 * 检查是否登录
 * @return {[type]} [description]
 */
function checkLogin() {

    return new Promise((resolve, reject) => {

        let token = window.localStorage.access_token;

        if (token) {
            Vue.http({
                url: CONF.API.checkIsLogin,
                method: 'GET',
                headers: {
                    'Authorization': 'Bearer ' + window.localStorage.access_token,
                }
            }).then(data => {

                console.log('mmmmm', data);

                let code = data.body.code;

                if (code === 200) {
                    resolve({status: true});
                } else {
                    resolve({status: false});
                }
            }, data => {
                resolve(new Error(data));
            });
        } else {

            resolve({status: false});
        }
        //resolve({status: true});
    });
}

/**
 * 微信授权
 * @return {[type]} [description]
 */
function authorize () {

    return new Promise((resolve, reject) => {

        let urlParams = Util.queryURLParams();

        // 取code成功，则换取access_tocken
        if (urlParams.code) {

            Server.queryAccessToken(urlParams.code).then(data => {

                window.localStorage.access_token = data.access_token;

                resolve({status: true});
            });

        } else {

            let wxUrl = "https://open.weixin.qq.com/connect/oauth2/authorize",
                appid = "wx64ed6345461d6fe2",
                redirect_uri= encodeURIComponent(window.location.href),
                response_type= "code",
                scope = "snsapi_userinfo",
                state = "STATE#wechat_redirect";

            window.location.href = `${wxUrl}?appid=${appid}&redirect_uri=${redirect_uri}&response_type=${response_type}&scope=${scope}&state${state}`;
        }
    });
}

/**
 * 激活账号，赋予已经登录的账号某种权限
 * @param  {[String]} a_secret a_secret
 * @param  {[String]} a_id     a_id
 * @return {[Promise]}
 */
function active (a_secret, a_id) {

    return new Promise((resolve, reject) => {
        Vue.http({
            url: CONF.API.activeUser,
            method: 'POST',
            body: {
                a_secret: a_secret,
                a_id: a_id
            },
            headers: {
                'Authorization': 'Bearer ' + window.localStorage.access_token,
            }
        }).then(data => {

            let code = data.body.code;

            if (code === 200) {

                resolve({status: true});
            } else {
                resolve({status: false});
            }

        }, data => {
            reject(new Error(data));
        });
    });
}

export {
    checkLogin,
    authorize,
    active
};
